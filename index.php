<?php

session_start();
define("bazydanych", "projekt");


$host = 'pokegym.cdoxgkwhone3.eu-central-1.rds.amazonaws.com';
$db = 'bazydanychproj';
$user = 'bazydanychproj';
$pass = 'bazydanychproj';

$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_EMULATE_PREPARES => false,];

$pdo = new PDO($dsn, $user, $pass, $opt);

function sqlparse($input){
    $input = trim($input);
    $input = strip_tags($input);
    $input = htmlspecialchars($input);
    if (get_magic_quotes_gpc()){
        $input = stripslashes($input);
    }
    $input = addcslashes($input, "%_");
    $input = trim($input);
    return $input;
}

if(isset($_GET['action']) && $_GET['action'] == "logout"){
    session_destroy();
    header("Location: index.php");
}

if(isset($_GET['action']) && $_GET['action'] == "load"){
    if(isset($_GET['get'])){
        switch($_GET['get']){
            case "bus":
                include_once("modules/get/bus.php");
                break;
            case "schedule":
                include_once("modules/get/schedule.php");
                break;
            case "route":
                include_once("modules/get/route.php");
                break;
            case "shift":
                include_once("modules/get/shift.php");
                break;
            case "stop":
                include_once("modules/get/stop.php");
                break;
            case "user":
                include_once("modules/get/user.php");
                break;
            default:
                http_response_code(404);
                break;
        }
        die();
    }
    if(isset($_GET['get_single'])){
        switch($_GET['get_single']){
            case "bus":
                include_once("modules/get_single/bus.php");
                break;
            case "schedule":
                include_once("modules/get_single/schedule.php");
                break;
            case "route":
                include_once("modules/get_single/route.php");
                break;
            case "shift":
                include_once("modules/get_single/shift.php");
                break;
            case "stop":
                include_once("modules/get_single/stop.php");
                break;
            default:
                http_response_code(404);
                break;
        }
        die();
    }
    if(isset($_GET['set'])){
        switch($_GET['set']){
            case "bus":
                include_once("modules/set/bus.php");
                break;
            case "stop":
                include_once("modules/set/stop.php");
                break;
            case "route":
                include_once("modules/set/route.php");
                break;
            case "schedule":
                include_once("modules/set/schedule.php");
                break;
            default:
                http_response_code(404);
                break;
        }
        die();
    }
}

if(isset($_POST['action']) && $_POST['action'] == "login"){

    //header("Content-type: text/plain");

    //print_r($_POST);

    $email = sqlparse($_POST['email']);
    $pass = sqlparse($_POST['password']);


    $orgstatement = 'SELECT user_id, email, name, surname, register_date, password, role FROM user WHERE email = ?';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute([$email]);

    $retval = $stmt->fetchAll();

    //print_r($retval);

    if(password_hash($pass, PASSWORD_DEFAULT) == $retval['password'] || $pass == $retval[0]['password']){
        // login OK

        foreach($retval[0] as $key => $val){
            $_SESSION[$key] = $val;
        }
        $_SESSION["logged_in"] = "true";
    }else{
        // wrong pass or sth

        $gbsmg = "Wprowadzono złe hasło!";
        include_once("modules/login.php");
        die();
    }
}

if(isset($_POST['action'])){
        switch($_POST['action']){
            case "set_bus":
                include_once("modules/post/bus.php");
                break;
            case "set_stop":
                include_once("modules/post/stop.php");
                break;
            case "set_route":
                include_once("modules/post/route.php");
                break;
            case "set_schedule":
                include_once("modules/post/schedule.php");
                break;
            case "login":
                if($_SESSION["role"] == 1){
                    include_once("modules/admin.php");
                }else{
                    include_once("modules/user.php");
                }
                break;
            default:
                http_response_code(404);
                break;
        }
        die();
}

if($_SESSION["logged_in"] == "true"){
    if($_SESSION["role"] == 1){
        include_once("modules/admin.php");
    }else{
        include_once("modules/user.php");
    }
}else{
    include_once("modules/login.php");
}

die();
//http_response_code(404);
//print_r($_SESSION);
