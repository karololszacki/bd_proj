<?php
    
if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

include_once(__DIR__ . "/head.php");

?>
</head>
<body>
    <div id="wrapper">

        <?php include_once(__DIR__ . "/nav.php") ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Panel główny</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row col-lg-12">
                <div class="col-lg-12 jumbotron">
                    <h4 class="text-center">&larr; Wybierz menu po lewej</h4>
              </div>
            </div>
        </div>
        <!-- /#page-wrapper  -->

    </div>
    <!-- /#wrapper  -->
<?php include_once(__DIR__ . "/tail.php"); ?>