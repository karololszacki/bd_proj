<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Przystanki</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php if(isset($_GET["removed"]) && !empty($_GET["removed"])){ ?>
                <div class="alert alert-info">
                    Przystanek usunięto.
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="pull-left"><h5>Tabela przystanków</h5></div>
                            <div class="pull-right">
                                <a class="btn btn-success" href="/index.php?action=load&set=stop"><i class="fa fa-plus"></i>&nbsp; Dodaj przystanek</a>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="przystanki-tabela">
                                <thead>
                                    <tr>
                                        <th>Numer przystanku</th>
                                        <th>Nazwa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                    $orgstatement = 'SELECT * FROM stop';

                                    $stmt = $pdo->prepare($orgstatement);

                                    $stmt->execute();

                                    $retval = $stmt->fetchAll();


                                    foreach($retval as $key => $val){
                                        echo "<tr>";
                                        echo "<td>".$val["stop_id"]."</td><td><a href='index.php?action=load&set=stop&id=". $val["stop_id"] . "' title='edytuj'>" .$val["stop_name"] . " </a></td>\n";
                                        echo "</tr>";
                                    }
                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#przystanki-tabela').DataTable({
            responsive: true,
            "language": {
                "url": "vendor/datatables/dataTables.polish.lang"
            }
        });
    });
    </script>

<?php
include_once(__DIR__ . "/../tail.php"); ?>
