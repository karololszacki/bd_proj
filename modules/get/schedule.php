<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

    $orgstatement = 'SELECT `ID_number` FROM `bus`';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $bus = $stmt->fetchAll();


    $orgstatement = 'SELECT * FROM `route`';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $route = $stmt->fetchAll();


    $orgstatement = 'SELECT `shift_num` FROM `shift`';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $shift = $stmt->fetchAll();



    $orgstatement = 'SELECT `name`, `surname`, `user_id` FROM `user` WHERE `role` = 0';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $users = $stmt->fetchAll();

    //print_r($bus);
    //print_r($route);
    //print_r($shift);


    include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<script>
    var bus, route, shift, week, userid;
    function show_modal(that, theuser){
        userid = theuser;
        week = $(that).parent().parent()[0].cells[0].innerText; // nr autobusu
        bus = $(that).parent().parent()[0].cells[1].innerText; // nr autobusu
        route = $(that).parent().parent()[0].cells[2].innerText; // trasa
        shift = $(that).parent().parent()[0].cells[4].innerText; // zmiana

        $("#bus").val(bus);
        $("#route").val(route);
        $("#shift").val(shift);
    }

    function modal_send(that){
        $.ajax({
                method: "POST",
                url: "/modules/ajax/schedule.php",
                data: {
                    old_bus: bus,
                    old_route: route,
                    old_shift: shift,
                    bus: $("#bus").val(),
                    route: $("#route").val(),
                    shift: $("#shift").val(),
                    week: week,
                    userid: userid
                }
            })
            .done(function( msg ) {
                console.info( "AJAX: Data Saved");
                window.location.replace("index.php?action=load&get=schedule");
            }).fail(function( jqXHR, textStatus ) {
                alert( "AJAX failed: " + textStatus );
        });
        $('#scheduleModal').modal('hide');
        return false;
    }
</script>

</head>
<body>

<div class="modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Edytuj dane
                </h4>
            </div>

            <form class="" role="form" onsubmit="event.preventDefault(); modal_send(this);">
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="bus">Wybierz autobus:</label>
                        <select class="form-control" id="bus">
                            <?php
                                foreach($bus as $key => $val) {
                                    foreach($val as $k => $v) {
                                        echo "<option>".$v."</option>";
                                    }
                                }
                            ?>
                        </select>
                        <br>
                    </div>


                    <div class="form-group">
                        <label for="route">Wybierz trasę:</label>
                        <select class="form-control" id="route">
                            <?php
                                foreach($route as $key => $val){
                                    echo "<option value='" . $val["route_id"] . "'>". $val["route_name"] . "</option>";
                                }
                            ?>
                        </select>
                        <br>
                    </div>

                    <div class="form-group">
                        <label for="shift">Wybierz zmianę:</label>
                        <select class="form-control" id="shift">
                            <?php
                                foreach($shift as $key => $val) {
                                    foreach($val as $k => $v) {
                                        echo "<option>".$v."</option>";
                                    }
                                }
                            ?>
                        </select>
                        <br>
                    </div>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-left">Wyślij</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                </div>
            </form>
        </div>
    </div>
</div>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Grafiki pracowników</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php

            foreach($users as $key => $val) {

                $orgstatement = 'SELECT s.week_num, s.user_id, u.name, u.surname, s.bus_id, s.route_id, r.route_name, s.shift_num FROM schedule s LEFT JOIN route r ON r.route_id = s.route_id LEFT JOIN user u ON u.user_id = s.user_id WHERE s.user_id = ?';

                $stmt = $pdo->prepare($orgstatement);
                //print_r($val["user_id"]);

                $stmt->execute([$val["user_id"]]);

                $schedule = $stmt->fetchAll();

                if (!empty($schedule)) {
                    //print_r($schedule);
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading clearfix">
                                    <div class="pull-left"><h5><?php echo $schedule[0]["name"] . " " . $schedule[0]["surname"]; ?></h5></div>
                                    <div class="pull-right">
                                        <a class="btn btn-success" href="/index.php?action=load&set=schedule&id=<?php echo $schedule[0]["user_id"]; ?>"><i class="fa fa-plus"></i>&nbsp; Dodaj kolejny tydzień</a>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <table width="100%" class="table table-striped table-bordered table-hover grafik-table">
                                        <thead>
                                        <tr>
                                            <th>Tydzień</th>
                                            <th>Nr autobusu</th>
                                            <th>Nr trasy</th>
                                            <th>Nazwa trasy</th>
                                            <th>Zmiana</th>
                                            <th>Edycja</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            foreach ($schedule as $keysch => $valsch) {
                                                echo "<tr>";
                                                foreach ($valsch as $column => $value) {
                                                    if($column != "name" && $column != "surname" && $column != "user_id"){
                                                        echo "<td>" . $value . "</td>";
                                                    }
                                                }
                                                echo "<td><a href='#' onclick='show_modal(this, ".$val["user_id"].")' data-toggle=\"modal\" data-target=\"#scheduleModal\">edytuj </a></td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                    <!-- /.table-responsive -->

                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <?php
                }else{ // wrap if
                    ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="pull-left"><h5><?php echo $val["name"] . " " . $val["surname"]; ?></h5></div>
                            <div class="pull-right">
                                <a class="btn btn-success" href="/index.php?action=load&set=schedule&id=<?php echo $val["user_id"]; ?>"><i class="fa fa-plus"></i>&nbsp; Dodaj kolejny tydzień</a>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                    </div>
                </div>
            </div>
            <?php
                } // wrap else-if
            } // wrap foreach
            ?>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    //$(document).ready(function() {
        $('.grafik-table').DataTable({
            responsive: true,
            "language": {
                "processing":     "Przetwarzanie...",
                "search":         "Szukaj:",
                "lengthMenu":     "Pokaż _MENU_ pozycji",
                "info":           "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty":      "Pozycji 0 z 0 dostępnych",
                "infoFiltered":   "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix":    "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords":    "Nie znaleziono pasujących pozycji",
                "emptyTable":     "Brak danych",
                "paginate": {
                    "first":      "Pierwsza",
                    "previous":   "Poprzednia",
                    "next":       "Następna",
                    "last":       "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            }
        });
    //});
    </script>

<?php
include_once(__DIR__ . "/../tail.php"); ?>
