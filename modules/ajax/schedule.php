<?php

session_start();
define("bazydanych", "projekt");


$host = 'pokegym.cdoxgkwhone3.eu-central-1.rds.amazonaws.com';
$db = 'bazydanychproj';
$user = 'bazydanychproj';
$pass = 'bazydanychproj';

$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_EMULATE_PREPARES => false,];

$pdo = new PDO($dsn, $user, $pass, $opt);

function sqlparse($input){
    $input = trim($input);
    $input = strip_tags($input);
    $input = htmlspecialchars($input);
    if (get_magic_quotes_gpc()){
        $input = stripslashes($input);
    }
    $input = addcslashes($input, "%_");
    $input = trim($input);
    return $input;
}

if($_SESSION["logged_in"] == "true"){
    if($_SESSION["role"] == 1){

        $bus = sqlparse($_POST['bus']);
        $route = sqlparse($_POST['route']);
        $shift = sqlparse($_POST['shift']);

        $old_bus = sqlparse($_POST['old_bus']);
        $old_route = sqlparse($_POST['old_route']);
        $old_shift = sqlparse($_POST['old_shift']);

        $week = sqlparse($_POST['week']);
        $userid = sqlparse($_POST['userid']);


        $orgstatement = "UPDATE schedule SET `bus_id` = ?, `route_id` = ?, `shift_num` = ? WHERE `bus_id` = ? AND `route_id` = ? AND `shift_num` = ? AND `week_num` = ? AND `user_id` = ?";

        $stmt = $pdo->prepare($orgstatement);

        $stmt->execute([$bus, $route, $shift, $old_bus, $old_route, $old_shift, $week, $userid]);

        die($stmt->errorInfo()); // all OK
    }
}

//die();
http_response_code(403);
//print_r($_SESSION);
