<?php

session_start();
define("bazydanych", "projekt");


$host = 'pokegym.cdoxgkwhone3.eu-central-1.rds.amazonaws.com';
$db = 'bazydanychproj';
$user = 'bazydanychproj';
$pass = 'bazydanychproj';

$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_EMULATE_PREPARES => false,];

$pdo = new PDO($dsn, $user, $pass, $opt);

function sqlparse($input){
    $input = trim($input);
    $input = strip_tags($input);
    $input = htmlspecialchars($input);
    if (get_magic_quotes_gpc()){
        $input = stripslashes($input);
    }
    $input = addcslashes($input, "%_");
    $input = trim($input);
    return $input;
}

if($_SESSION["logged_in"] == "true"){
    if($_SESSION["role"] == 1){

        $order = sqlparse($_POST['order']);
        $routeid = sqlparse($_POST['routeid']);
        $stopid = sqlparse($_POST['stopid']);


        $orgstatement = "UPDATE route_stop SET `order` = ? WHERE route_id = ? AND stop_id = ?";

        $stmt = $pdo->prepare($orgstatement);

        $stmt->execute([$order, $routeid, $stopid]);

        die($stmt->errorInfo()); // all OK
    }
}

//die();
http_response_code(403);
//print_r($_SESSION);
