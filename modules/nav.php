<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

?>
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Panel Administracyjny Systemu Transportu Miejskiego</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="text-info"><?php echo $_SESSION["name"] . " " . $_SESSION["surname"]; ?></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil użytkownika</a>
                        <li><a href="?action=logout"><i class="fa fa-sign-out fa-fw"></i> Wyloguj się</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hide">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Szukaj...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i>Panel sterowania</a>
                        </li>
                        <?php
                            if($_SESSION["role"] == 1){
                        ?>
                        <li>
                            <a href="?action=load&get=schedule"><i class="fa fa-table fa-fw"></i> Grafik pracowników</a>
                        </li>
                        <li>
                            <a href="?action=load&get=route"><i class="fa fa-table fa-fw"></i> Trasy</a>
                        </li>
                        <li>
                            <a href="?action=load&get=stop"><i class="fa fa-table fa-fw"></i> Przystanki</a>
                        </li>
                        <li>
                            <a href="?action=load&get=bus"><i class="fa fa-table fa-fw"></i> Autobusy</a>
                        </li>
                        <li>
                            <a href="?action=load&get=shift"><i class="fa fa-table fa-fw"></i> Zmiany</a>
                        </li>
                        <?php
                            }else{
                        ?>
                        <li>
                            <a href="?action=load&get_single=schedule"><i class="fa fa-table fa-fw"></i> Mój grafik</a>
                        </li>
                        <li>
                            <a href="?action=load&get_single=route"><i class="fa fa-table fa-fw"></i> Trasy</a>
                        </li>
                        <li>
                            <a href="?action=load&get_single=stop"><i class="fa fa-table fa-fw"></i> Przystanki</a>
                        </li>
                        <li>
                            <a href="?action=load&get_single=bus"><i class="fa fa-table fa-fw"></i> Autobusy</a>
                        </li>
                        <li>
                            <a href="?action=load&get_single=shift"><i class="fa fa-table fa-fw"></i> Zmiany</a>
                        </li>
                        <?php
                            }
                        ?>
                        <?php
                            if($_SESSION["role"] == 1){
                                ?>
                                <li>
                                    <a href="?action=load&get=user"><i class="fa fa-table fa-fw"></i> Użytkownicy</a>
                                </li>
                                <?php
                            }
                        ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
