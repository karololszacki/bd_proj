<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

if(isset($_GET['id'])){
    $stop_id = sqlparse($_GET['id']);

    $orgstatement = 'SELECT stop_name FROM stop WHERE stop_id = ?';
    $stmt = $pdo->prepare($orgstatement);
    $stmt->execute([$stop_id]);

    $stop_name = $stmt->fetchAll();
    $stop_name = $stop_name[0]["stop_name"];


    $orgstatement = 'SELECT r.route_name FROM `route_stop` rs LEFT JOIN route r ON r.`route_id` = rs.`route_id` WHERE stop_id = ?';
    $stmt = $pdo->prepare($orgstatement);
    $stmt->execute([$stop_id]);

    $route_names = $stmt->fetchAll();

}else{
    $stop_id = $stop_name = "";
}

include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <script>
    function usun(){
        $.ajax({
                method: "POST",
                url: "/modules/ajax/stop.php",
                data: { stop_id: $("#numer").val() }
            })
            .done(function( msg ) {
                console.info( "AJAX: stop removed");
                window.location.replace("index.php?action=load&get=stop&removed=true");
            }).fail(function( jqXHR, textStatus ) {
            alert( "AJAX failed: " + textStatus );
        });
        return false;
    }
    </script>

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Przystanki</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php if($_SESSION["role"] == 1) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php
                                    if (isset($_GET['id'])) {
                                        echo "Zmień przystanek";
                                    } else {
                                        echo "Dodaj przystanek";
                                    }
                                ?>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form role="form" class="form-horizontal" method="POST" action="index.php">
                                            <input type="hidden" id="action" name="action" value="set_stop">
                                            <?php if (isset($_GET['id'])) { ?>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="numer">Numer
                                                        przystanku</label>
                                                    <div class="col-sm-9">
                                                        <input type="number" id="numer" name="numer"
                                                               class="form-control" placeholder="Numer przystanku"
                                                               value="<?php echo $stop_id; ?>" readonly>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="name">Nazwa
                                                    przystanku</label>
                                                <div class="col-sm-9">
                                                    <input type="text" id="name" name="name" class="form-control"
                                                           placeholder="Nazwa przystanku"
                                                           value="<?php echo $stop_name; ?>">
                                                </div>
                                            </div>
                                            <?php if (isset($_GET['id'])) { ?>
                                                <button type="submit" class="btn btn-primary">Zaktualizuj</button>
                                                <button type="button" class="btn btn-danger" onclick="usun()">Usuń
                                                </button>
                                                <button type="reset" class="btn btn-warning">Resetuj formularz</button>
                                            <?php } else { ?>
                                                <button type="submit" class="btn btn-primary">Wyślij</button>
                                                <button type="reset" class="btn btn-warning">Wyczyść formularz</button>
                                            <?php } ?>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <?php
            }
            if(isset($_GET['id'])){
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Informacje o przystanku <?php echo $stop_id . " (" . $stop_name . ")"; ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="well">
                        <strong>Trasy, na których znajduje się przystanek <?php echo $stop_id . " (" . $stop_name . ")"; ?>:</strong>
                        <?php
                        $str = "";
                        foreach($route_names as $key => $val){
                            foreach($val as $k => $v){
                                $str .= $v . ", ";
                            }
                        }
                        echo substr($str, 0, -2);
                        ?>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php
include_once(__DIR__ . "/../tail.php"); ?>
