<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}


    $orgstatement = 'SELECT ID_number FROM bus';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $bus = $stmt->fetchAll();


    $orgstatement = 'SELECT * FROM route';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $route = $stmt->fetchAll();


    $orgstatement = 'SELECT shift_num FROM shift';

    $stmt = $pdo->prepare($orgstatement);

    $stmt->execute();

    $shift = $stmt->fetchAll();

    $userid = sqlparse($_GET['id']);

    $orgstatement = 'SELECT * FROM `user` WHERE user_id = ?';
    $stmt = $pdo->prepare($orgstatement);
    $stmt->execute([$userid]);

    $user = $stmt->fetchAll();
    $user = $user[0]["name"] . " " . $user[0]["surname"];


include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Grafik</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Ustal grafik kierowcy<?php echo ": " . $user; ?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" class="form-horizontal" method="POST" action="index.php">
                                        <input type="hidden" id="action" name="action" value="set_schedule">
                                        <input type="hidden" id="userid" name="userid" value="<?php echo $userid; ?>">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="week">Numer tygodnia</label>
                                            <div class="col-sm-8">
                                                <input type="number" id="week" name="week" class="form-control" placeholder="" value="" aria-describedby="numberHelp">
                                            </div>
                                        </div>
                                        <div class="alert alert-warning">
                                            <small id="numberHelp" class="form-text">
                                                <strong>Ostrzeżenie!</strong> Podanie złego (zduplikowanego) numeru tygodnia nadpisze wcześniej wprowadzone dane
                                            </small>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="bus">Wybierz autobus:</label>
                                            <div class="col-sm-8"><select class="form-control" id="bus" name="bus">
                                                <?php
                                                    foreach($bus as $key => $val) {
                                                        foreach($val as $k => $v) {
                                                            echo "<option>".$v."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="route">Wybierz trasę:</label>
                                            <div class="col-sm-8"><select class="form-control" id="route" name="route">
                                                <?php
                                                    foreach($route as $key => $val){
                                                        echo "<option value='" . $val["route_id"] . "'>". $val["route_name"] . "</option>";
                                                    }
                                                ?>
                                            </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="shift">Wybierz zmianę:</label>
                                            <div class="col-sm-8"><select class="form-control" id="shift" name="shift">
                                                <?php
                                                    foreach($shift as $key => $val) {
                                                        foreach($val as $k => $v) {
                                                            echo "<option>".$v."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Wyślij</button>
                                        <button type="reset" class="btn btn-warning"><?php if(isset($_GET['id'])){ ?>Resetuj<?php }else{ ?>Wyczyść<?php } ?> formularz</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php
include_once(__DIR__ . "/../tail.php"); ?>
