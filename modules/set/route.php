<?php

    if(!defined('bazydanych') || bazydanych != "projekt"){
        die("Security breach");
    }

    if(isset($_GET['id'])){
        $route_id = sqlparse($_GET['id']);

        $orgstatement = 'SELECT route_name FROM route WHERE route_id = ?';
        $stmt = $pdo->prepare($orgstatement);
        $stmt->execute([$route_id]);

        $route_name = $stmt->fetchAll();
        $route_name = $route_name[0]["route_name"];

        $orgstatement = 'SELECT rs.order, s.stop_id, s.stop_name FROM `route_stop` rs LEFT JOIN stop s ON s.stop_id = rs.stop_id WHERE route_id = ? ORDER BY `rs`.`order` ASC';
        $stmt = $pdo->prepare($orgstatement);
        $stmt->execute([$route_id]);

        $stops_id = $stmt->fetchAll();

    }else{
        $route_id = "";
    }

    include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables more CSS -->
    <link href="vendor/datatables-rowreorder/css/rowReorder.bootstrap.min.css" rel="stylesheet">

    </head>
    <body>

<div id="wrapper">

    <?php include_once(__DIR__ . "/../nav.php") ?>


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Trasy</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <?php if($_SESSION["role"] == 1) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php
                                if (isset($_GET['id'])) {
                                    echo "Zmień trasę";
                                } else {
                                    echo "Dodaj trasę";
                                }
                            ?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" class="form-horizontal" method="POST" action="index.php">
                                        <input type="hidden" id="action" name="action" value="set_route">
                                        <?php if (isset($_GET['id'])) { ?>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="numer">Numer trasy</label>
                                                <div class="col-sm-9">
                                                    <input type="number" id="numer" name="numer" class="form-control"
                                                           placeholder="Numer trasy" value="<?php echo $route_id; ?>"
                                                           readonly>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="name">Nazwa trasy</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="name" name="name" class="form-control"
                                                       placeholder="Nazwa trasy" value="<?php echo $route_name; ?>">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Wyślij</button>
                                        <button type="reset"
                                                class="btn btn-warning"><?php if (isset($_GET['id'])) { ?>Resetuj<?php } else { ?>Wyczyść<?php } ?>
                                            formularz
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php
        }
            if(isset($_GET['id'])){
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Informacje o trasie nr. <?php echo $route_id . " (" . $route_name . ")"; ?></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                            <div class="alert alert-info">Przeciągnij przestanki, by zmienić ich kolejność na trasie</div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="trasy-tabela">
                                <thead>
                                <th>Kolejność przystanku</th>
                                <th>Nr przystanku</th>
                                <th>Nazwa przystanku</th>
                                </thead>
                                <tbody>
                            <?php
                                foreach($stops_id as $key => $val){
                                    echo "<tr>";
                                    foreach($val as $k => $v){
                                        echo "<td>".$v."</td>";
                                    }
                                    echo "</tr>\n";
                                }
                            ?>
                                </tbody>
                            </table>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- DataTables JavaScript -->
<script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="vendor/datatables-rowreorder/js/dataTables.rowReorder.min.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        var table = $('#trasy-tabela').DataTable({
            <?php if($_SESSION["role"] == 1) { ?>
            rowReorder: true,
            <?php } ?>
            "language": {
                "url": "vendor/datatables/dataTables.polish.lang"
            }
        });
        <?php if($_SESSION["role"] == 1) { ?>
        table.on( 'row-reorder', function ( e, diff, edit ) {
            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                var rowData = table.row( diff[i].node ).data();

                //console.log(rowData[1] + ' updated to be in position '+ diff[i].newData);
                console.log("UPDATE route_stop SET `order` = "+ diff[i].newData + " WHERE route_id = <?php echo $route_id; ?> AND stop_id = " + rowData[1]);

                $.ajax({
                        method: "POST",
                        url: "/modules/ajax/route.php",
                        data: { order: diff[i].newData, routeid: <?php echo $route_id; ?>, stopid: rowData[1] }
                    })
                    .done(function( msg ) {
                        console.info( "AJAX: Data Saved");
                    }).fail(function( jqXHR, textStatus ) {
                        alert( "AJAX failed: " + textStatus );
                    });
            }
        } );
        <?php } ?>
    });
</script>

<?php
    include_once(__DIR__ . "/../tail.php"); ?>
