<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

if(isset($_GET['id'])){
    $bus_id = sqlparse($_GET['id']);

    $orgstatement = 'SELECT * FROM bus WHERE ID_number = ?';
    $stmt = $pdo->prepare($orgstatement);
    $stmt->execute([$bus_id]);

    $bus_arr = $stmt->fetchAll();
    $bus = $bus_arr[0];

}else{
    $bus = [];
    $bus["buy_date"] = $bus["next_check_date"] = $bus["insurance_validation_date"] = "";
}

include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Autobusy</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Dodaj autobus
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" class="form-horizontal" method="POST" action="index.php">
                                    <input type="hidden" id="action" name="action" value="set_bus">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="numer">Numer autobusu</label>
                                            <div class="col-sm-8">
                                                <input type="number" id="numer" name="numer" class="form-control" placeholder="Number boczny pojazdu" value="<?php echo $bus_id; ?>" aria-describedby="numberHelp">
                                            </div>
                                        </div>
                                        <div class="alert alert-warning">
                                            <?php if(isset($_GET['id'])){ ?>
                                                <small id="numberHelp" class="form-text">
                                                    <strong>Ostrzeżenie!</strong> Zmiana numeru autobusu może nadpisać dane już istniejącego pojazdu
                                                </small>
                                            <?php }else{ ?>
                                                <small id="numberHelp" class="form-text">
                                                    <strong>Ostrzeżenie!</strong> Podanie złego (zduplikowanego) numeru autobusu nadpisze wcześniej wprowadzone dane
                                                </small>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="buydate">Data zakupu</label>
                                            <div class="col-sm-8">
                                                <input type="date" id="buydate" name="buydate" class="form-control datepicker" value="<?php echo $bus["buy_date"]; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="checkdate">Data następnego przeglądu</label>
                                            <div class="col-sm-8">
                                                <input type="date" id="checkdate" name="checkdate" class="form-control datepicker" value="<?php echo $bus["next_check_date"]; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="validdate">Data ważności ubezpieczenia</label>
                                            <div class="col-sm-8">
                                                <input type="date" id="validdate" name="validdate" class="form-control datepicker" value="<?php echo $bus["insurance_validation_date"]; ?>">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Wyślij</button>
                                        <button type="reset" class="btn btn-warning"><?php if(isset($_GET['id'])){ ?>Resetuj<?php }else{ ?>Wyczyść<?php } ?> formularz</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php
include_once(__DIR__ . "/../tail.php"); ?>
