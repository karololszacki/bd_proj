<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Trasy</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                         <div class="panel-heading clearfix">
                            <div class="pull-left"><h5>Tabela tras</h5></div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="trasy-tabela">
                                <thead>
                                    <tr>
                                        <th>Nr trasy</th>
                                        <th>Nazwa</th>
                                        <th>Przyst. 1</th>
                                        <th>Przyst. 2</th>
                                        <th>Przyst. 3</th>
                                        <th>Przyst. 4</th>
                                        <th>Przyst. 5</th>
                                        <th>Przyst. 6</th>
                                        <th>Przyst. 7</th>
                                        <th>Przyst. 8</th>
                                        <th>Przyst. 9</th>
                                        <th>Przyst. 10</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                $orgstatement = 'SELECT * FROM route';

                                $stmt = $pdo->prepare($orgstatement);

                                $stmt->execute();

                                $route = $stmt->fetchAll();

                                //print_r($retval);

                                for($i = 1; $i <= count($route); $i++){

                                    echo "\n<tr>";
                                    //echo "<td>" . $route[$i - 1]['route_id'] . "</td>" . "<td>" . $route[$i - 1]['route_name'] . "</td>\n";

                                    echo "<td>" . $route[$i - 1]['route_id'] . "</td>";
                                    echo "<td><a href='index.php?action=load&set=route&id=". $route[$i - 1]['route_id'] . "' title='edytuj'>" .$route[$i - 1]['route_name'] . " </a></td>\n";
                                    //echo "<td>" . $route[$i - 1]['route_name'] . "</td>\n";


                                    $orgstatement = 'SELECT s.stop_id as stopid, s.stop_name as stopname FROM route_stop rs LEFT JOIN stop s ON rs.stop_id = s.stop_id WHERE rs.route_id = '.$i.' ORDER BY rs.order';

                                    $stmt = $pdo->prepare($orgstatement);

                                    $stmt->execute();

                                    $retval = $stmt->fetchAll();

                                    //print_r($retval);


                                    foreach($retval as $key => $val){
                                        echo "<td><a href='index.php?action=load&set=stop&id=". $val["stopid"] . "'>" .$val["stopname"] . " </a></td>\n";
                                    }
                                    for($a = 0; $a < 10 - count($retval); $a++){
                                        echo "<td></td>";
                                    }
                                    echo "</tr>\n";
                                }

                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#trasy-tabela').DataTable({
            responsive: true,
            "language": {
                "url": "vendor/datatables/dataTables.polish.lang"
            }
        });
    });
    </script>

<?php
include_once(__DIR__ . "/../tail.php"); ?>
