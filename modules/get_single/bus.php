<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Autobusy</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php if(isset($gbsmg2) && !empty($gbsmg2)){ ?>
                <div class="alert alert-info">
                  <?php print_r($gbsmg2); ?>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="pull-left"><h5>Tabela autobusów</h5></div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="autobusy-tabela">
                                <thead>
                                    <tr>
                                        <th>Numer</th>
                                        <th>Data zakupu</th>
                                        <th>Data następnego przeglądu</th>
                                        <th>Data ważności ubezpieczenia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                    $orgstatement = 'SELECT * FROM bus';

                                    $stmt = $pdo->prepare($orgstatement);

                                    $stmt->execute();

                                    $retval = $stmt->fetchAll();

                                    /*
                                    foreach($retval as $key => $val){
                                        echo "<tr>";
                                        foreach($val as $column => $value){
                                            echo "<td>" . $value . "</td>";
                                        }
                                        echo "</tr>";
                                    }
                                    */
                                    foreach($retval as $key => $val){
                                        echo "<tr>";
                                        echo "<td>".$val["ID_number"]."</td>\n";
                                        echo "<td>".$val["buy_date"]."</td>\n";
                                        echo "<td>".$val["next_check_date"]."</td>\n";
                                        echo "<td>".$val["insurance_validation_date"]."</td>\n";
                                        echo "</tr>";
                                    }
                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#autobusy-tabela').DataTable({
            responsive: true,
            "language": {
                "url": "vendor/datatables/dataTables.polish.lang"
            }
        });
    });
    </script>

<?php
include_once(__DIR__ . "/../tail.php"); ?>
