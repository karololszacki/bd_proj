<?php

if(!defined('bazydanych') || bazydanych != "projekt"){
    die("Security breach");
}

include_once(__DIR__ . "/../head.php");

?>
    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>
<body>

    <div id="wrapper">

        <?php include_once(__DIR__ . "/../nav.php") ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Zmiany</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                         <div class="panel-heading clearfix">
                            <div class="pull-left"><h5>Tabela zmian, w których mogą pracować kierowcy</h5></div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="zmiany-tabela">
                                <thead>
                                    <tr>
                                        <th>Numer zmiany</th>
                                        <th>Początek zmiany</th>
                                        <th>Koniec zmiany</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                    $orgstatement = 'SELECT * FROM shift';

                                    $stmt = $pdo->prepare($orgstatement);

                                    $stmt->execute();

                                    $retval = $stmt->fetchAll();


                                    foreach($retval as $key => $val){
                                        echo "<tr>";
                                        foreach($val as $column => $value){
                                            echo "<td>" . $value . "</td>";
                                        }
                                        echo "</tr>";
                                    }
                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#zmiany-tabela').DataTable({
            responsive: true,
            "language": {
                "url": "vendor/datatables/dataTables.polish.lang"
            }
        });
    });
    </script>

<?php
include_once(__DIR__ . "/../tail.php"); ?>
