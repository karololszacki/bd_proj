<?php

    ignore_user_abort(true);
    set_time_limit(0);

    if ($_GET['secret'] != 'tXDw7uENsXm369k8cNB3NxCU') {
        die("fail");
    }

    header('Content-Type: text/plain');
    putenv('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin');

    $branch = "master";

    exec("git checkout " . $branch, $out1);
    exec("git pull 2>&1", $out2);

    die();
